# Upstream Containers Downstream

Publish upstream container images downstream without [upstream container registry traffic limits](https://www.docker.com/increase-rate-limits).

## How to release particular upstream image downstream

Tag this repository according following scheme: `<upstream-app-name>/<upstream-app-container-tag/version>`.

Examples:
 * to release `prom/node-exporter:v1.1.2` to `registry.gitlab.ics.muni.cz:443/cloud/upstream-containers-downstream/node-exporter:v1.1.2` use tag [`node-exporter/v1.1.2`](https://gitlab.ics.muni.cz/cloud/upstream-containers-downstream/-/tags/node-exporter%2Fv1.1.2)
 * to release `gcr.io/cadvisor/cadvisor:v0.39.0` to `registry.gitlab.ics.muni.cz:443/cloud/upstream-containers-downstream/cadvisor:v0.39.0` use tag [`cadvisor/v0.39.0`](https://gitlab.ics.muni.cz/cloud/upstream-containers-downstream/-/tags/cadvisor%2Fv0.39.0)
 * to release `cytopia/ansible-lint:5-0.6` to `registry.gitlab.ics.muni.cz:443/cloud/upstream-containers-downstream/ansible-lint:5-0.6` use tag [`ansible-lint/5-0.6`](https://gitlab.ics.muni.cz/cloud/upstream-containers-downstream/-/tags/ansible-lint%2F5-0.6)
